/*Materia: Algoritmos y esctructuras de datos.
Profesor titular: Mg. Eduardo Nicolas Campazzo
Ciclo lectivo: 2016*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h> /*Archivo de cabezara. Sirve para utilizar funciones que, trabajan sobre arrays de caracteres.*/

/*Definicion de macros. Esto es util para cuando se quiere cambiar el tama�o de
un array de manera rapida y sin errores, ya que cambiando el valor de la macro
se reemplaza, en todas aquellas partes del programa donde fueron usadas.*/
#define NOM 51
#define APE 31
#define TILLA 30
#define DIR 51

/*Estructura*/
struct contacto
{
    char nombre[NOM];
    long int tel;
    char correo[APE];
    char direccion[DIR];
}plantilla[TILLA]; /*Variable de estructura, para acceder a sus campos se utiliza el operador punto "."*/

/*Prototipos de funciones. Sirven para que el compilador pueda hacer
 una fuerte comprobacion de tipos, no es obligatorio en c.Sin embargo en c++ si lo es.*/
void agregar(void);
int comprobar(void);
int buscar(void);
void mostrar(void);

int main()
{
    int opcion, clave;

    /*Menu del programa. La sentencia "do while" es muy utilizada para la creacion de menus, esta
    se combina con la sentencia "switch".*/
    do
    {
      printf("\t\t\t\tAgenda U.N.LaR. 2016\n");
      printf("\t\t\t\t--------------------");

      printf("\n\tMenu\n");
      printf("1. Agregar contactos\n");
      printf("2. Buscar contactos\n");
      printf("3. Mostrar contactos\n");
      printf("4. Borrar contacto\n");
      printf("5. Salir\n");
      scanf("%d", &opcion);
      fflush(stdin);
      switch(opcion)

     {
         case 1:
             agregar();
             break;
         case 2:
             clave=buscar();
             if(clave!=-1)
             {
                 printf("\nNombre: %s\n",plantilla[clave].nombre);
                 printf("Telefono%ld\n",plantilla[clave].tel);
                 printf("Correo%s\n",plantilla[clave].correo);
                 printf("Direccion%s\n",plantilla[clave].direccion);
             }
             else
             {
                 printf("\nContacto inexistente\n");
             }
             break;
         case 3:
             mostrar();
             break;
         case 4:
         case 5:
             printf("\nHasta luego\n\n");
             break;
         default:{
             printf("\nOpcion incorrecta. Intente de nuevo\n\n");
         }
     }
    }while(opcion!=5);
    return 0;

}

void agregar(void)
{
    int depende;
    depende=comprobar();

    if(depende==-1)
    {
        printf("\nAgenda llena\n");
        return;
    }

    printf("Nombre: ");
    gets(plantilla[depende].nombre);

    printf("Telefono: ");
    scanf("%ld",&plantilla[depende].tel);
    fflush(stdin);

    printf("Correo: ");
    gets(plantilla[depende].correo);

    printf("Domicilio: ");
    gets(plantilla[depende].direccion);
}

int comprobar(void)
{
    register int llena;
    for(llena=0;plantilla[llena].nombre[0]&&llena<TILLA;++llena);
	if(llena==TILLA)
		return -1; /*No hay espacio en mi estructura*/
	return llena; /*Retorna al primer espacio libre que encuentra en mi array de estructuras*/
}

int buscar(void)
{
    int i;
    char palabra[NOM];
    printf("\nIngrese el nombre buscado: \n");
    gets(palabra);
    for(i=0;i<30;i++)
    {
        if(strcmp(palabra,plantilla[i].nombre)==0)
        {
            return i;
        }
    }
    return -1;
}

void mostrar(void)
{
    int i;

    for(i=0;i<TILLA;i++)
    {
        printf("\n\nContacto %d\n",i+1);
        printf("Nombre: %s\n",plantilla[i].nombre);
        printf("Telefono: %ld\n",plantilla[i].tel);
        printf("Correo: %s\n",plantilla[i].correo);
        printf("Direccion: %s\n",plantilla[i].direccion);
    }
}

void borrar(void)
{
    int thc;
    thc=buscar();

    if(thc!=-1)
    {
        plantilla[thc].nombre[0]='\0';/*Se asigna el valor nulo al primer elemento del campo "nombre",
                                        porque actua en concordancia con la funcion "Comprobar"*/
        printf("\nContacto borrado\n");
    }
    else
    {
        printf("\n�Atencion! El contacto que quiere borrar no existe\n");
    }
}


